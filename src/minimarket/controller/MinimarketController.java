/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minimarket.controller;

import java.sql.Connection;
import dao.daoMinimarket;
import dao.implementMinimarket;
import javax.swing.JOptionPane;
import minimarket.model.MinimarketModel;
import minimarket.view.MinimarketView;

/**
 *
 * @author ALFINA
 */
public class MinimarketController {
    private MinimarketModel model;
    private implementMinimarket iminimarket;
   
    public void setModel(MinimarketModel model){
        this.model = model;
    }
    
    public void resetForm(MinimarketView view){
        String kode_barang = view.getTxtkode_barang().getText();
        String nama_barang = view.getTxtnama_barang().getText();
        String stok_barang = view.getTxtstok().getText();
        String harga_satuan = view.getTxtharga_Satuan().getText();
        String jumlah_jual = view.getTxtjumlah_jual().getText();
        String harga_akhir = view.getTxtharga_akhir().getText();
        
        if(kode_barang.equals("") && nama_barang.equals("") && stok_barang.equals("") && harga_satuan.equals("") && jumlah_jual.equals("") && harga_akhir.equals("")){
        }else{
            model.resetForm();
        }
    }
    
    public void simpanForm(MinimarketView view){
        
        String kode_barang = view.getTxtkode_barang().getText();
        String nama_barang = view.getTxtnama_barang().getText();
        String stok_barang = view.getTxtstok().getText();
        String harga_satuan = view.getTxtharga_Satuan().getText();
        String jumlah_jual = view.getTxtjumlah_jual().getText();
        String harga_akhir = view.getTxtharga_akhir().getText();
        
        model.setKode_barang(kode_barang);
        model.setNama_barang(nama_barang);
        model.setStok_barang(stok_barang);
        model.setHarga_satuan(harga_satuan);
        model.setJumlah_jual(jumlah_jual);
        model.setHarga_akhir(harga_akhir);
        
        if(kode_barang.trim().equals("")){
            JOptionPane.showMessageDialog(view, "Email masih kosong!");
        }else if(nama_barang.trim().equals("")){
            JOptionPane.showMessageDialog(view, "No Telepon masih kosong");
        }else if(stok_barang.trim().equals("")){
            JOptionPane.showMessageDialog(view, "No Telepon masih kosong");
        }else if(harga_satuan.trim().equals("")){
            JOptionPane.showMessageDialog(view, "No Telepon masih kosong");
        }else if(jumlah_jual.trim().equals("")){
            JOptionPane.showMessageDialog(view, "No Telepon masih kosong");
        }else if(harga_akhir.trim().equals("")){
            JOptionPane.showMessageDialog(view, "No Telepon masih kosong");
        }else{
            iminimarket = new daoMinimarket();
            iminimarket.insert(model);
            JOptionPane.showMessageDialog(view, "Data Sudah Masuk Ke Database");
            model.resetForm();
        }
    }
}
