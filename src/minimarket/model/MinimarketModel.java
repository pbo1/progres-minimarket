/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minimarket.model;

import javax.swing.JOptionPane;
import minimarket.event.MinimarketListener;

/**
 *
 * @author ALFINA
 */
public class MinimarketModel {
    private String kode_barang;
    private String nama_barang;
    private String stok_barang;
    private String harga_satuan;
    private String jumlah_jual;
    private String harga_akhir;
    
    private MinimarketListener minimarketListener ;

    public MinimarketListener getMinimarketListener() {
        return minimarketListener;
    }

    public void setMinimarketListener(MinimarketListener minimarketListener) {
        this.minimarketListener = minimarketListener;
    }

    public String getKode_barang() {
        return kode_barang;
    }

    public void setKode_barang(String kode_barang) {
        this.kode_barang = kode_barang;
        fireOnChange();
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
        fireOnChange();
    }

    public String getStok_barang() {
        return stok_barang;
    }

    public void setStok_barang(String stok_barang) {
        this.stok_barang = stok_barang;
        fireOnChange();
    }

    public String getHarga_satuan() {
        return harga_satuan;
    }

    public void setHarga_satuan(String harga_satuan) {
        this.harga_satuan = harga_satuan;
        fireOnChange();
    }

    public String getJumlah_jual() {
        return jumlah_jual;
    }

    public void setJumlah_jual(String jumlah_jual) {
        this.jumlah_jual = jumlah_jual;
        fireOnChange();
    }

    public String getHarga_akhir() {
        return harga_akhir;
    }

    public void setHarga_akhir(String harga_akhir) {
        this.harga_akhir = harga_akhir;
        fireOnChange();
    }

    
    
    protected void fireOnChange(){
        if(minimarketListener != null)
        {
            minimarketListener.onChange(this);
        }
    }
  
    public void resetForm(){
        setKode_barang("");
        setNama_barang("");
        setStok_barang("");
        setHarga_satuan("");
        setJumlah_jual("");
        setHarga_akhir("");
    }
    
    public void simpanForm(){
        JOptionPane.showMessageDialog(null, "Berhasil disimpan");
        resetForm(); //mereset form setelah tekan simpan
    }

}
