/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import minimarket.model.MinimarketModel;
import minimarket.model.koneksi;

/**
 *
 * @author ALFINA
 */
public class daoMinimarket implements implementMinimarket {
    Connection con;
    final String insert = "INSERT INTO tblminimarket (kode_barang, nama_barang, stok, harga_satuan, jumlah_jual, harga_akhir) VALUES (?, ?, ?, ?, ?, ?);";
    
    public daoMinimarket(){
        con = koneksi.connection();
    }

    
    public void insert(MinimarketModel model){
        PreparedStatement st = null;
        
        try{
            st = con.prepareStatement(insert);
            st.setString(1, model.getKode_barang());
            st.setString(2, model.getNama_barang());
            st.setString(3, model.getStok_barang());
            st.setString(4, model.getHarga_satuan());
            st.setString(5, model.getJumlah_jual());
            st.setString(6, model.getHarga_akhir());
            
            
            st.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            try{
                st.close();
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
    }

    private void connection() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
